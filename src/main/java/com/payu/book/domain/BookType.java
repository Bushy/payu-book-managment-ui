package com.payu.book.domain;

public enum BookType {
    HARD_COVER, SOFT_COVER, EBOOK, OTHER
}
