package com.payu.book.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Book {

    private Long id;
    @NotBlank
    private String name;
    @NotBlank
    private String isbnNumber;

    private String type;
    @NumberFormat
    private BigDecimal price;

}
