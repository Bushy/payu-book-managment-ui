package com.payu.book.controller;

import javax.validation.Valid;

import com.payu.book.domain.Book;
import com.payu.book.domain.BookType;
import com.payu.book.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/books/")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @GetMapping("signup")
    public String showSignUpForm(Book book) {
        return "add-book";
    }

    @GetMapping("list")
    public String showUpdateForm(Model model) {
        model.addAttribute("books", bookService.findAll());
        return "index";
    }

    @PostMapping("add")
    public String addBook(@Valid Book book, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-book";
        }
        bookService.save(book);
        return "redirect:list";
    }

    @GetMapping("edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Book book = bookService.findById(id);
        model.addAttribute("book", book);
        return "update-book";
    }

    @PostMapping("update/{id}")
    public String updateBook(@PathVariable("id") long id, @Valid Book book, BindingResult result, Model model) {
        if (result.hasErrors()) {
            book.setId(id);
            return "update-book";
        }
        bookService.save(book);
        model.addAttribute("books", bookService.findAll());
        return "index";
    }

    @GetMapping("delete/{id}")
    public String deleteBook(@PathVariable("id") long id, Model model) {
        bookService.deleteById(id);
        model.addAttribute("books", bookService.findAll());
        return "index";
    }
}
