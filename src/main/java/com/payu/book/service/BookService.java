package com.payu.book.service;

import com.payu.book.client.BooksClient;
import com.payu.book.domain.Book;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.Boolean.TRUE;
import static java.util.Objects.nonNull;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookService {
    private final BooksClient booksClient;

    public Book save(Book book) {
        return getResponse(booksClient.save(book), "save");
    }

    public Book update(Book book, Long id) {
        return getResponse(booksClient.update(book, id), "update");
    }

    public Book findById(Long id) {
        return getResponse(booksClient.findById(id), "findById");
    }

    public boolean existsById(Long id) {
        return TRUE.equals(getResponse(booksClient.existsById(id), "existsById"));
    }

    public List<Book> findAll() {
        return getResponse(booksClient.findAll(), "findAll");
    }

    public long count() {
        Long count = getResponse(booksClient.count(), "count");
        return count == null ? 0 : count;
    }

    public void deleteById(Long id) {
        booksClient.deleteById(id);
    }

    private <R> R getResponse(ResponseEntity<R> responseEntity, String operation) {
        if (!responseEntity.getStatusCode().is2xxSuccessful() && nonNull(responseEntity.getBody())) {
            log.error("{} failed with => status code : {}", operation, responseEntity.getStatusCode());
            return null;
        } else {
            R entityBody = responseEntity.getBody();
            log.info("success entityBody response : {}", entityBody);
            return entityBody;
        }
    }

}
