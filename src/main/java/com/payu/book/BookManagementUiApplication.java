package com.payu.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class BookManagementUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookManagementUiApplication.class, args);
	}

}
