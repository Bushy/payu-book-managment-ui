package com.payu.book.client;

import com.payu.book.domain.Book;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@FeignClient(name = "books-api", url = "http://localhost:8080/payu-books-api/books")
public interface BooksClient {

    @PostMapping("/add")
    ResponseEntity<Book> save(@RequestBody Book book);

    @PutMapping("/update/{id}")
    ResponseEntity<Book> update(@RequestBody Book book, @PathVariable Long id);

    @GetMapping("/{id}")
    ResponseEntity<Book> findById(@PathVariable Long id);

    @GetMapping("/exists/{id}")
    ResponseEntity<Boolean> existsById(@PathVariable Long id);

    @GetMapping("/all")
    ResponseEntity<List<Book>> findAll();

    @GetMapping("/count")
    ResponseEntity<Long> count();


    @DeleteMapping("delete/{id}")
    ResponseEntity<Void> deleteById(@PathVariable Long id);
}
